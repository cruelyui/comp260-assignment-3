﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodProducer : MonoBehaviour
{
    public int ID;
    public KeyCode _code=KeyCode.Space;
    private Food _food;
    // Start is called before the first frame update
    void Start()
    {
        _food = ConfigManager._foodconfig.FindFoodById(ID);
     
           
        GetComponent<SpriteRenderer>().sprite = _food.icon;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            isEnter = true;


        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isEnter = false;


        }
    }
    bool isEnter = false;
    // Update is called once per frame
    void Update()
    {
        if(isEnter)
        {
            if (Input.GetKeyDown(_code))
            {
                Debug.Log("sb");

                if (Scene._player.GetComponent<PlayerController>()._handfood != null)
                    return;
                
                Scene._player.GetComponent<PlayerController>().PickUp(_food);
                _food = ConfigManager._foodconfig.FindFoodById(ID);
            }
        }
    }
}
