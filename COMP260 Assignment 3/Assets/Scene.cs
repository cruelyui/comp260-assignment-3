﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class Scene :MonoBehaviour
{
    public int Level;
    public GameObject OverView;
    public SpriteRenderer Menu_Sprite;
    public Text time_text;
    public Text score_text;
    public  List<int> Food_ID=new List<int>();
    private  float _score;
    public  float _time;
    float _timer;
    public static  GameObject _player;
    private void Awake()
    {
        _timer = _time;
           _player = GameObject.FindGameObjectWithTag("Player");
        CreatNewFood();
    }
    public float GetScore()
    {
        return _score;
    }
    private void Update()
    {
        
        if(_timer<=0)
        {
            OverView.SetActive(true);
        }
        else
        {
            _timer -= Time.deltaTime;
        }
        time_text.text = "Time: " + (int)_timer;
        score_text.text = "Score: " + _score;
        
    }
    public void CreatNewFood()
    {
        int[] id = new int[] { 1,6,5};
        Food_ID.Add(id[Random.Range(0, Level)]);
        Menu_Sprite.sprite = null;
        Menu_Sprite.DOFade(0, 0.5f).SetEase(Ease.Linear);
        Menu_Sprite.DOFade(1, 0.5f).SetEase(Ease.Linear).SetDelay(0.5f);
        Timer.Register(0.5f, () => { Menu_Sprite.sprite = ConfigManager._foodconfig.FindFoodById(Food_ID[0]).icon; });
      
    }
    public  void AddScore(float score)
    {
        _score += score;
    }

    public  void DecreaseTime(float time)
    {
        _timer -= time;
    }
}
