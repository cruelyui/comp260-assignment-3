﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[System.Serializable]
public  class Food  
{
    public int id;
    public Sprite icon;
    public string name;

    
}
[System.Serializable]
public class CookedFood:Food
{
    public float CookTime;
    public Food CooedFood;
    [NonSerialized]
    public bool isRaw=true;

    public  Food Clone()
    {
        CookedFood f = new CookedFood();
        f.id = id;
        f.icon = icon;
        f.name = name;
        f.CookTime = CookTime;
        f.CooedFood = this.CooedFood;
        f.isRaw = true;
        return f;
    }
}
[System.Serializable]
public class MakeUpFood : Food
{
    public List<int> foods_ids=new List<int>();
}


 
