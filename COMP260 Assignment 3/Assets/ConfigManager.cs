﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigManager    
{
    private static FoodConfig tempfood=null;
    public static FoodConfig _foodconfig
    {
        get
        {
            if(tempfood==null)
            {
                tempfood = Resources.Load<FoodConfig>("FoodConfig");
            }
            return tempfood;
        }
        set
        {
            tempfood = value;
        }
    }
}
