﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PlayerController : MonoBehaviour
{
    [NonSerialized]
    public Food _handfood;
    public SpriteRenderer _hand_food;
    public float _speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void PickUp(Food food)
    {
        _handfood = food;
       
        if (food is CookedFood)
        {
        
            if (!(food as CookedFood).isRaw)
            {
                _hand_food.sprite = (food as CookedFood).CooedFood.icon;
                return;
            }
        }
       
        _hand_food.sprite = food.icon;
    }
    public GameObject PutDown(Transform pos)
    {
        GameObject temp=   Instantiate(_hand_food.gameObject,pos);
        temp.transform.localScale = Vector3.one;
        temp.transform.localPosition = Vector3.zero;
        _handfood = null;
        _hand_food.sprite = null;
        return temp;
    }
    public void Move()
    {
     
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        if(h<0)
        {
            transform.localScale = Vector3.one*1.5f;
        }
        if(h>0)
        {
            transform.localScale = new Vector3(-1.5f, 1.5f,1.5f);
        }
        transform.Translate(new Vector2(h, v) * Time.deltaTime * _speed);
    }
    // Update is called once per frame
    void Update()
    {
        Move();
    }
}
