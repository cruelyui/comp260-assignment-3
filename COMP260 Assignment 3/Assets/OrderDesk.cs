﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderDesk : MonoBehaviour
{
    private List<Food> _food = new List<Food>();
    public Transform _point;
    public KeyCode code;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isEnter = true;


        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isEnter = false;


        }
    }
    bool isEnter = false;
    private void Update()
    {
        if (isEnter)
        {
            if (Input.GetKeyDown(code))
            {
                if (Scene._player.GetComponent<PlayerController>()._handfood != null)
                {
                    _food.Add(Scene._player.GetComponent<PlayerController>()._handfood);
                    Scene._player.GetComponent<PlayerController>().PutDown(_point);

                    if(_food[0].id==Level_1_Scene._instance.Food_ID[0])
                    {
                        _food.RemoveAt(0);

                        Destroy(_point.transform.GetChild(0).gameObject,1);
                        Level_1_Scene._instance.CreatNewFood();
                        Level_1_Scene._instance.AddScore(10);
                        Level_1_Scene._instance.Food_ID.RemoveAt(0);
                    }
                    else
                    {
                        _food.RemoveAt(0);
                        Destroy(_point.transform.GetChild(0).gameObject, 1);
                        
                        Level_1_Scene._instance.DecreaseTime(5);
                    }
                }
                else if (Scene._player.GetComponent<PlayerController>()._handfood == null && _point.transform.childCount > 0)
                {
                    Destroy(_point.transform.GetChild(0).gameObject);
                    Scene._player.GetComponent<PlayerController>().PickUp(_food[0]);
                    _food.RemoveAt(0);
                }
            }
        }
    }
}
