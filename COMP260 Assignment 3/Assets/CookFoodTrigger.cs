﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

 
public class CookFoodTrigger : MonoBehaviour
{
    private CookedFood _food;
    public Image process;
    bool isStartCook;
    float timer;
    public KeyCode code;
    GameObject temp;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isEnter = true;


        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isEnter = false;


        }
    }
    bool isEnter = false;
    
    // Update is called once per frame
    void Update()
    {
        if(isEnter)
        {
            if (Input.GetKeyDown(code))
            {
                if (Scene._player.GetComponent<PlayerController>()._handfood != null)
                {
                  if(  _food != null)
                        return;

                    if (!(Scene._player.GetComponent<PlayerController>()._handfood is CookedFood))
                        return;

                    if (!(Scene._player.GetComponent<PlayerController>()._handfood as CookedFood).isRaw)
                        return;

                    _food = Scene._player.GetComponent<PlayerController>()._handfood as CookedFood;
                    isStartCook = true;
                    process.gameObject.transform.parent.gameObject.SetActive(true);
                    temp = Scene._player.GetComponent<PlayerController>().PutDown(transform);

                }
                else
                {
                    if (_food != null)
                    {
                        if (timer >= _food.CookTime)
                        {

                            Destroy(temp);
                            timer = 0;
                            isStartCook = false;

                            Scene._player.GetComponent<PlayerController>().PickUp(_food);
                            _food = null;

                        }
                    }
                }
            }
        }
        
            if (isStartCook)
            {
            
                timer += Time.deltaTime;
                process.fillAmount = timer / _food.CookTime;
                if(timer>=_food.CookTime)
                {
                _food.id = _food.CooedFood.id;
                process.gameObject.transform.parent.gameObject.SetActive(false);
                _food.isRaw = false;
                temp.GetComponent<SpriteRenderer>().sprite = _food.CooedFood.icon;
               isStartCook = false;
                }
            
            }
        
    }
}
