﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 

public class PutDownDesk : MonoBehaviour
{
    private List<Food> _food=new List<Food>();
    public Transform _point;
    public KeyCode code;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isEnter = true;


        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isEnter = false;


        }
    }
    bool isEnter = false;
    private void Update()
    {
        if(isEnter)
        {
            if (Input.GetKeyDown(code))
            {
                if (Scene._player.GetComponent<PlayerController>()._handfood != null)
                {
                    _food.Add(Scene._player.GetComponent<PlayerController>()._handfood);
                    Scene._player.GetComponent<PlayerController>().PutDown(_point);
                   
                    

                        for (int i = 0; i < ConfigManager._foodconfig.makeupfoods.Count; i++)
                        {
                        bool CantMakeUp = false;
                        for (int j = 0; j< ConfigManager._foodconfig.makeupfoods[i].foods_ids.Count; j++)
                        {
                            if (!_food.Exists(a => { return a.id == ConfigManager._foodconfig.makeupfoods[i].foods_ids[j]; }))
                            {
                                CantMakeUp = true;
                                break ;
                            }
                        }

                        if(!CantMakeUp)
                        {



                            List<Food> index_list = new List<Food>();
                            foreach (var item in ConfigManager._foodconfig.makeupfoods[i].foods_ids)
                            {
                                int index= _food.FindIndex(a => { return a.id == item; });
                                Destroy(_point.transform.GetChild(index).gameObject);
                                index_list.Add(_food.Find(a => { return a.id == item; }));


                            }
                            foreach (var item in index_list)
                            {

                                _food.Remove(item);
                            }


                         

                            Food f = ConfigManager._foodconfig.makeupfoods[i];
                            _food.Add( f);
                             
                            GameObject temp = Instantiate(Scene._player.GetComponent<PlayerController>(). _hand_food.gameObject, _point);
                            temp.transform.localPosition = Vector3.zero;
                            temp.transform.localScale = Vector3.one;
                            temp.GetComponent<SpriteRenderer>().sprite = f.icon;
                        }

                        }
                    

                
                   
                }
                else if (Scene._player.GetComponent<PlayerController>()._handfood == null && _point.transform.childCount > 0)
                {
                    Destroy(_point.transform.GetChild(0).gameObject);
                    Scene._player.GetComponent<PlayerController>().PickUp(_food[0]);
                    _food.RemoveAt(0);
                }
            }
        }
    }
}
