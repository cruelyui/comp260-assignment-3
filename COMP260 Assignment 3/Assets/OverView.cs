﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class OverView : MonoBehaviour {

    public Text scoretext;
    public GameObject[] starts;
	// Use this for initialization
	void Start () {
        scoretext.text = Level_1_Scene._instance.score_text.text;
        if (Level_1_Scene._instance.GetScore()>=20)
        {
            starts[0].SetActive(true);
        }
        if (Level_1_Scene._instance.GetScore() >= 40)
        {
            starts[1].SetActive(true);
        }
        if (Level_1_Scene._instance.GetScore() >= 60)
        {
            starts[2].SetActive(true);
        }
    }
	public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Ok()
    {
        SceneManager.LoadScene("Level");
    }
    // Update is called once per frame
    void Update () {
		
	}
}
