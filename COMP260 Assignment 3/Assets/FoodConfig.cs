﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FoodConfig")]
public class FoodConfig : ScriptableObject
{
    public List<MakeUpFood> makeupfoods = new List<MakeUpFood>();
    public List<CookedFood> cookedfoods = new List<CookedFood>();
    public List<Food> foods = new List<Food>();

    public Food FindFoodById(int id)
    {

        foreach (var item in makeupfoods)
        {
            if (item.id == id)
                return item;
        }
        foreach (var item in cookedfoods)
        {
            if (item.id == id)
                return item.Clone();

            if (item.CooedFood.id == id)
                return item.CooedFood;
        }
        foreach (var item in foods)
        {
            if (item.id == id)
                return item;
        }
        return null;
    }
}

